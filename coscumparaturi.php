<?php

   require_once 'header.php';
   require_once 'meniu.php';
   require_once 'footer.php';
   echo $header;
   echo $meniu;
   echo $footer;
  
  
?>
   
<?php

if(isset($_POST["add"])){
   if(isset($_SESSION["index"])){
      $item_array_id=array_column($_SESSION["index"], $column= "product_id");
      if(!in_array($GET["id"],$item_array_id)){
         $count=count($_SESSION["index"]);
         $item_array=array(
            'product_id' => $_GET["id"],
            'item_name' => $_POST["name"],
            'product_price' => $_POST["price"],
            'item_quantity' => $_POST["quantity"],

         );
         $_SESSION["index"][$count]=$item_array;
         echo '<script>window.location="index.php"</script>';
      }
      else{
         echo '<script> alert("Produsul a fost deja adaugat in cos")</script>';
         echo '<script>window.location="index.php"</script>';
      }
   }
      else{
         $item_array=array(
            'product_id' => $_GET["id"],
            'item_name' => $_POST["name"],
            'product_price' => $_POST["price"],
            'item_quantity' =>$_POST["quantity"],

         );
         $_SESSION["index"][0]=$item_array;
      }
      }
   if(isset($_GET["action"])){
      if($_GET["action"]=="delete"){
         foreach($_SESSION["index"] as $keys => $value){
            if($value["product_id"]== $_GET["id"]){
               unset($_SESSION["index"][$keys]);
               echo '<script>alert("Produsul a fost sters!")<?script>';
               echo '<script>window.location="index.php"</script>';
            }
         }
      }
   }
 

?>




<div class="container">

<div style="clear:both"></div>
<h3 class="title3" 
    style="text-align: center;
    color: #66afe9;
    background-color: #efefef;
    padding: 2%;">Cosul meu</h3>
<div class="table-responsive">
   <table class="table table-bordered">
<tr>
<th width="30%">Numele produsului</th>
<th width="10%">Cantitate</th>
<th width="13%">Pret</th>
<th width="10%">Total</th>
<th width="17%">Eliminati un produs</th>
</tr>
 <?php

   if(!empty($_SESSION["index"])){
       $total=0;
       foreach ($_SESSION["index"] as $key => $value){
           ?>
    <tr>
    <td><?php echo $value["item_name"];?></td>
    <td><?php echo $value["item_quantity"];?></td>
    <td>$ <?php echo $value["product_price"]; ?> </td>
    <td>$ <?php echo number_format($number= $value["item_quantity"]* $value["product_price"], $decimals=2); ?></td>
    <td><a href="index.php?action=delete&id=<?php echo $value["product_id"]; ?>"><span class="text-danger">Remove</span></a></td>
    </tr>
    <?php
    $total=$total+ ($value["item_quantity"]*$value["product_price"]);
       }
    ?>
    <tr>
    <td colspan="3" align="right">Total</td>
    <th align="right">$<?php echo number_format($total.$decimals=2); ?> </th>
    <td></td>
    </tr>
    <?php
       }
       ?>

      </table>
</div>
</div>